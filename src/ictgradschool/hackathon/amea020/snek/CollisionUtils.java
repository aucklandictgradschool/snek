package ictgradschool.hackathon.amea020.snek;

import java.util.Collection;
import java.util.Collections;

/**
 * A utility class which contains some static methods to do with tile-based collision detection.
 */
public class CollisionUtils {

    /**
     * Returns a value indicating whether the first game object collides with the second.
     * @param o1 the first object to check
     * @param o2 the second object to check
     * @return true if a collision happened, false otherwise
     */
    public static boolean collide(IGameObject o1, IGameObject o2) {
        return !Collections.disjoint(o1.getOccupiedTiles(), o2.getOccupiedTiles());
    }

    /**
     * Returns a value indicating whether the given game object collides with any of the objects in the collection.
     * @param obj the object to check
     * @param objCollection the collection to check
     * @return true if a collision happened, false otherwise
     */
    public static boolean collide(IGameObject obj, Collection<? extends IGameObject> objCollection) {

        for(IGameObject obj2 : objCollection) {
            if (collide(obj, obj2)) {
                return true;
            }
        }

        return false;

    }

}
