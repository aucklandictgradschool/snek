package ictgradschool.hackathon.amea020.snek;

/**
 * Deals with configuration values / constants / etc.
 */
public class Config {

    private static Config instance;

    /**
     * A singleton instance of Config. There can be only one.
     *
     * This is an example of the singleton pattern, which can be handy if you know you'll only ever want one of a
     * particular kind of object and you want to share it widely amongst your classes. However, it's not considered
     * best practice, so use it sparingly.
     *
     * @return the singleton instance of Config.
     */
    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    /** The size of each tile, in pixels */
    private int tileSize = 32;

    /** The width of the screen, in tiles */
    private int screenWidthInTiles = 35;

    /** The height of the screen, in tiles */
    private int screenHeightInTiles = 25;

    /** The initial length of a snek */
    private int initialSnekLength = 3;

    /** The tileset used to draw image graphics */
    private SnekTileset tileset = new SnekTileset("snek-graphics.png", 64, tileSize);

    /** The number of points awarded for blocking an opposing snek in multiplayer, causing them do die */
    private int killBonusPoints = 1000;

    /** Gets the size of each tile, in pixels */
    public int getTileSize() {
        return this.tileSize;
    }

    /** Gets the width of the screen, in tiles */
    public int getScreenWidthInTiles() {
        return screenWidthInTiles;
    }

    /** Gets the height of the screen, in tiles */
    public int getScreenHeightInTiles() {
        return screenHeightInTiles;
    }

    /** Gets the width of the screen, in pixels */
    public int getScreenWidthInPixels() {
        return getScreenWidthInTiles() * getTileSize();
    }

    /** Gets the height of the screen, in pixels */
    public int getScreenHeightInPixels() {
        return getScreenHeightInTiles() * getTileSize();
    }

    /** Gets the tileset used to render images in the game */
    public SnekTileset getTileset() {
        return tileset;
    }

    /** Gets the initial length of a snek */
    public int getInitialSnekLength() {
        return initialSnekLength;
    }

    /** Gets the number of points awarded for blocking an opposing snek in multiplayer, causing them do die */
    public int getKillBonusPoints() {
        return killBonusPoints;
    }
}
