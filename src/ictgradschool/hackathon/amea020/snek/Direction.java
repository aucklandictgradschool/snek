package ictgradschool.hackathon.amea020.snek;

import java.awt.*;

/**
 * Represents directions you can move onscreen
 *
 * This represents a more advanced Enum syntax which we didn't cover in class. Basically, we gan give our enums
 * a constructor and fields and methods just like a normal class, and then specify the values passed into that
 * constructor when we define the different enum constants.
 *
 * Note that Enum constructors are only accessible inside the enum iteslf. For example, "Direction d = new Direction(...)"
 * is not valid code to place anywhere in your program.
 *
 * For more information, see: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
 */
public enum Direction {

    Up(0, -1),
    Down(0, 1),
    Left(-1, 0),
    Right(1, 0),
    None(0, 0);

    /** Represents the offset by which an entity will move when it moves in this direction. */
    private final Point delta;

    /**
     * Creates a new Direction.
     *
     * @param deltaX
     * @param deltaY
     */
    Direction(int deltaX, int deltaY) {
        this.delta = new Point(deltaX, deltaY);
    }

    /** Gets the offset by which an entity will move when it moves in this direction. */
    public Point getDelta() {
        return delta;
    }

    /**
     * Gets a value indicating whether the other direction is opposite to this one.
     *
     * For example, if this is Up, then the opposite would be Down, etc.
     *
     * @param other the other Direction to check
     * @return true if opposite, false otherwise
     */
    public boolean isOppositeTo(Direction other) {
        return (delta.x == other.delta.x && delta.y == -other.delta.y)
                || (delta.y == other.delta.y && delta.x == -other.delta.x);
    }
}
