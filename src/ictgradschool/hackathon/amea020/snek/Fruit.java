package ictgradschool.hackathon.amea020.snek;

import java.awt.*;
import java.util.Collection;
import java.util.Collections;

/**
 * Represents a fruit that sneks can eat. Each fruit gives the player a certain number of points when eaten, and may
 * cause the snek who ate it to grow a certain amount.
 */
public class Fruit implements IGameObject {

    /** Represents all configuration options. Kept in a central place. */
    private final Config cfg = Config.getInstance();

    /** The position of the fruit onscreen. */
    private Point position;

    /** The type of fruit. Determines its value in points, and how long it makes sneks grow. */
    private FruitType type;

    /**
     * Creates a new fruit with a random type.
     *
     * @param position the position of the fruit onscreen
     */
    public Fruit(Point position) {
        this.position = position;
        this.type = FruitType.values()[(int)(Math.random() * FruitType.values().length)];
    }

    /** Draws the fruit using the given Graphics object. A fruit's look is determined by its type. */
    public void draw(Graphics g) {
        cfg.getTileset().drawFruit(position, type, g);
    }

    /** Gets the position of the fruit onscreen. */
    public Point getPosition() {
        return position;
    }

    /** Sets the position of the fruit onscreen. */
    public void setPosition(Point position) {
        this.position = position;
    }

    /**
     * Gets a list of all tiles occupied by this fruit. Used for collision detection. All fruit simply occupy a single
     * tile - the one at its position.
     *
     * Collections.singleton is a quick way of creating a list with only one item, if you know you'll never need
     * to add more items to it.
     */
    @Override
    public Collection<Point> getOccupiedTiles() {
        return Collections.singleton(position);
    }

    /** Gets the type of fruit. Determines its value in points, and how long it makes sneks grow. */
    public FruitType getType() {
        return type;
    }

    /** Sets the type of fruit. Determines its value in points, and how long it makes sneks grow. */
    public void setType(FruitType type) {
        this.type = type;
    }
}
