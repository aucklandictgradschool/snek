package ictgradschool.hackathon.amea020.snek;

import java.awt.*;
import java.util.Collection;

/**
 * Represents some object in the game which can be involved in collision detection.
 */
public interface IGameObject {

    /**
     * Gets a list of all tiles this game object occupies.
     * @return
     */
    Collection<Point> getOccupiedTiles();

}
