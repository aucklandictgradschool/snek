package ictgradschool.hackathon.amea020.snek;

/**
 * Classes implementing this interface can be notified when various game events occur, such as when players
 * increase their score, or when sneks are killed.
 */
public interface ISnekGameListener {

    /**
     * Called when some event occurs in-game that observers might be interested in.
     *
     * @param e describes the event
     */
    void onSnekGameEvent(SnekGameEvent e);
}
