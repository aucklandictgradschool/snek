package ictgradschool.hackathon.amea020.snek;

/**
 * Classes implementing this interface can be notified when the "game over" panel has been shown on the SnekGamePanel
 * screen, and the user has pressed the "OK" button.
 */
public interface ISnekGamePanelListener {

    void gameFinished(SnekGamePanel panel);

}
