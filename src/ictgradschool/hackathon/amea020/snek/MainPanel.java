package ictgradschool.hackathon.amea020.snek;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Hosts the main game flow, switching between the title and game screens, etc.
 *
 */
public class MainPanel extends JPanel {

    private static final String TITLE_PANEL = "TitlePanel";
    private static final String GAME_PANEL = "GamePanel";

    /**
     * The main layout. CardLayout allows us to easily switch between different full-screen views, such as
     * the title page and the game screen.
     */
    private CardLayout layout;

    /** The title page, which has options such as "new game", etc. */
    private SnekTitlePanel titlePanel;

    /** The panel which displays an in-progress game. */
    private SnekGamePanel gamePanel;

    /**
     * Creates the panel and initially shows the title screen.
     */
    public MainPanel() {

        layout = new CardLayout();
        this.setLayout(layout);

        this.titlePanel = new SnekTitlePanel();
        // These are lambda expressions. See https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
        titlePanel.getBtnSinglePlayer().addActionListener((e) -> createGame(1));
        titlePanel.getBtnMultiPlayer().addActionListener((e) -> createGame(2));
        titlePanel.getBtnQuit().addActionListener((e) -> System.exit(0));
        this.add(titlePanel, TITLE_PANEL);

        titlePanel.startAnimation();
    }

    /** Disables all controls to stop user interaction with the buttons on this page. */
    private void disableControls() {
        titlePanel.stopAnimation();
        titlePanel.setEnabled(false);
        titlePanel.getBtnMultiPlayer().setEnabled(false);
        titlePanel.getBtnSinglePlayer().setEnabled(false);
        titlePanel.getBtnQuit().setEnabled(false);
    }

    /** Enables all controls to allow user interaction with the buttons on this page. */
    private void enableControls() {
        titlePanel.startAnimation();
        titlePanel.setEnabled(true);
        titlePanel.getBtnMultiPlayer().setEnabled(true);
        titlePanel.getBtnSinglePlayer().setEnabled(true);
        titlePanel.getBtnQuit().setEnabled(true);
    }

    /**
     * Creates a new game with the given number of players.
     *
     * For each player, this method will open a dialog box allowing that player to choose their name and color.
     * If all players do this, a new game will start. Otherwise, this method does nothing.
     *
     * @param numPlayers the number of players who will play the game.
     */
    private void createGame(int numPlayers) {

        disableControls();

        List<Player> players = new ArrayList<>();
        for (int i = 1; i <= numPlayers; i++) {

            // PlayerDetailsDialog extends JDialog, which is a special kind of JFrame.
            // When we call setVisible(true), this method will block until the user has clicked the Ok or Cancel
            // button on the dialog.
            PlayerDetailsDialog dialog = new PlayerDetailsDialog((JFrame) SwingUtilities.getRoot(this), "Player " + i);
            dialog.setLocationRelativeTo(MainPanel.this);
            dialog.setVisible(true);

            // Once the dialog has been closed (either ok or cancel), we'll reach this point in the code.

            if (dialog.isCancelled()) {
                enableControls();
                return;
            }

            players.add(new Player(dialog.getPlayerName(), dialog.getPlayerColor(), PlayerControls.values()[i-1]));

        }

        SnekGame game = new SnekGame(players);
        gamePanel = new SnekGamePanel(game);
        add(gamePanel, GAME_PANEL);
        layout.show(MainPanel.this, GAME_PANEL);
        gamePanel.startGame();

        // We can use lambda expressions on our own listener types too (e.g. ISnekGamePanelListener). Java is smart
        // enough to allow this for any interface containing only one method.
        gamePanel.setListener((g) -> {
            remove(gamePanel);
            gamePanel = null;
            enableControls();
            layout.show(MainPanel.this, TITLE_PANEL);
        });


    }

}
