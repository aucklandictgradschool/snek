package ictgradschool.hackathon.amea020.snek;

import java.awt.event.KeyEvent;

/**
 * Represents a player of the game.
 *
 * Each player has a Snek, a score, and a set of keyboard inputs which they can use to control the Snek.
 */
public class Player {

    /** The player's snek */
    private Snek snek;

    /** The player's score */
    private int score;

    /** The player's name */
    private final String name;

    /** The player's keyboard control mappings */
    private final PlayerControls controls;

    /** Each tick, we'll set the snek's direction to this value if it's not null. */
    private Direction snekNextDirection;

    /** The player's color. */
    private final SnekColor color;

    /**
     * Creates a new Player object.
     *
     * @param name the player's name
     * @param controls the player's keyboard control mappings
     */
    public Player(String name, SnekColor color, PlayerControls controls) {

        this.name = name;
        this.score = 0;
        this.controls = controls;
        this.color = color;

    }

    /** Gets the player's name */
    public String getName() {
        return name;
    }

    /** Gets the player's snek */
    public Snek getSnek() {
        return snek;
    }

    /** Sets the player's snek. This should be called before the game starts. */
    public void setSnek(Snek snek) {
        this.snek = snek;
    }

    /** Gets the player's score */
    public int getScore() {
        return score;
    }

    /** Gets the player's color */
    public SnekColor getColor() {
        return color;
    }

    /**
     * Adds the given amount to the player's score
     *
     * @param amount the amount to add
     */
    public void incrementScore(int amount) {
        score += amount;
    }

    /**
     * To be called once each game loop.
     * Appropriately sets the snek's direction based on the most recent user input, then moves the snek.
     */
    public void tick() {

        if (snekNextDirection != null) {
            snek.setDirection(snekNextDirection);
            snekNextDirection = null;
        }

        snek.move();

    }

    /**
     * Handles the given keyboard input.
     *
     * Returns a value indicating whether the input was handled by this player.
     *
     * @param e the {@link KeyEvent} to handle
     * @return true if the event was handled by this player, false otherwise.
     */
    public boolean handleKeyPress(KeyEvent e) {

        Direction newDirection = controls.getDirection(e.getKeyCode());
        if (newDirection != null) {
            if (!newDirection.isOppositeTo(snek.getDirection())) {
                snekNextDirection = newDirection;
            }

            return true;
        }

        return false;
    }
}
