package ictgradschool.hackathon.amea020.snek;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Enumerates different control mappings for different players.
 *
 * For an explanation of Java's complex enum syntax, refer to the Direction enum.
 */
public enum  PlayerControls {

    /** Player one has the standard Up, Down, Left, Right arrow controls. */
    PlayerOne(KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT),

    /** Player two has the W, S, A, D letter key controls. */
    PlayerTwo(KeyEvent.VK_W, KeyEvent.VK_S, KeyEvent.VK_A, KeyEvent.VK_D);

    /** Maps swing keycodes to directions. */
    private final Map<Integer, Direction> directionControlMap;

    /**
     * Creates a new PlayerControls.
     *
     * @param upKey the code for the key which should correspond to the Up direction.
     * @param downKey the code for the key which should correspond to the Down direction.
     * @param leftKey the code for the key which should correspond to the Left direction.
     * @param rightKey the code for the key which should correspond to the Right direction.
     */
    PlayerControls(int upKey, int downKey, int leftKey, int rightKey) {

        this.directionControlMap = new HashMap<>();
        directionControlMap.put(upKey, Direction.Up);
        directionControlMap.put(downKey, Direction.Down);
        directionControlMap.put(leftKey, Direction.Left);
        directionControlMap.put(rightKey, Direction.Right);

    }

    /**
     * Gets the {@link Direction} associated with the given key code, or null if there is none.
     *
     * @param keyCode
     * @return
     */
    public Direction getDirection(int keyCode) {

        if (directionControlMap.containsKey(keyCode)) {
            return directionControlMap.get(keyCode);
        }
        return null;
    }
}
