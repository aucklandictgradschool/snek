package ictgradschool.hackathon.amea020.snek;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * A frame that allows a player to choose their name and color.
 */
public class PlayerDetailsDialog extends JDialog {

    /** The textbox which allows a player to enter their name. */
	private JTextField txtName;

	/** The combo box which allows a player to choose their color. */
	private JComboBox<SnekColor> cboColors;

	/** The label which will display a colored snek corresponding to the player's currently chosen color. */
	private JLabel lblSnek;

	/** Whether or not the form is cancelled. */
	private boolean isCancelled = true;

	/**
	 * Create the frame.
     *
     * Code here is adapted from code created by Eclipse WindowBuilder.
	 */
	public PlayerDetailsDialog(JFrame owner, String defaultName) {
	    super(owner, "Player Details", true);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 161);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 100, 100};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0, 1.0, 0, 0};
		gbl_contentPane.rowWeights = new double[]{0.0, 0, 0.0};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblNewLabel = new JLabel("Enter your name:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		txtName = new JTextField(defaultName);
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.gridwidth = 3;
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 0;
		contentPane.add(txtName, gbc_txtName);
		txtName.setColumns(10);
		
		JLabel lblChooseYourColor = new JLabel("Choose your color:");
		GridBagConstraints gbc_lblChooseYourColor = new GridBagConstraints();
		gbc_lblChooseYourColor.anchor = GridBagConstraints.EAST;
		gbc_lblChooseYourColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblChooseYourColor.gridx = 0;
		gbc_lblChooseYourColor.gridy = 1;
		contentPane.add(lblChooseYourColor, gbc_lblChooseYourColor);

        cboColors = new JComboBox<>();
		SnekColor[] colors = SnekColor.values();
		for (SnekColor c : colors) {
            cboColors.addItem(c);
        }
        cboColors.setSelectedIndex(0);
		GridBagConstraints gbc_cboColors = new GridBagConstraints();
        gbc_cboColors.gridwidth = 3;
        gbc_cboColors.insets = new Insets(0, 0, 5, 0);
        gbc_cboColors.fill = GridBagConstraints.HORIZONTAL;
        gbc_cboColors.gridx = 1;
        gbc_cboColors.gridy = 1;
		contentPane.add(cboColors, gbc_cboColors);
		// A lambda expression. See https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
		cboColors.addItemListener((e) ->
		    lblSnek.setIcon(new ImageIcon(((SnekColor)cboColors.getSelectedItem()).getMiniSnekImage())));
		
		lblSnek = new JLabel();
		lblSnek.setIcon(new ImageIcon(colors[0].getMiniSnekImage()));
		GridBagConstraints gbc_lblSnek = new GridBagConstraints();
		gbc_lblSnek.insets = new Insets(0, 0, 0, 5);
		gbc_lblSnek.gridx = 0;
		gbc_lblSnek.gridy = 2;
		contentPane.add(lblSnek, gbc_lblSnek);
		
		JButton btnOk = new JButton("OK");
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 2;
		gbc_btnOk.gridy = 2;
		contentPane.add(btnOk, gbc_btnOk);
		btnOk.addActionListener((e) -> {
            isCancelled = false;
            setVisible(false);
        });
		
		JButton btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCancel.gridx = 3;
		gbc_btnCancel.gridy = 2;
		contentPane.add(btnCancel, gbc_btnCancel);
        btnCancel.addActionListener((e) -> {
            isCancelled = true;
            setVisible(false);
        });

		pack();
	}

	/** Gets a value indicating whether the user cancelled the dialog. */
    public boolean isCancelled() {
        return isCancelled;
    }

    /** Gets the name of the player, as typed in the text box. */
    public String getPlayerName() {
        return txtName.getText();
    }

    /**Gets the player's color, as selected in the text box. */
    public SnekColor getPlayerColor() {
        return (SnekColor) cboColors.getSelectedItem();
    }
}
