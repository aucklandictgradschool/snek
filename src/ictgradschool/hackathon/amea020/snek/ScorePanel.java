package ictgradschool.hackathon.amea020.snek;

import javax.swing.*;
import java.awt.*;

/**
 * A panel intended to display the score of a single player.
 */
public class ScorePanel extends JPanel {

    /** The player whose score to display */
    private final Player player;

    /** The label displaying the score */
    private final JLabel lblPlayerScore;

    /**
     * Creates a new ScorePanel.
     *
     * @param player the player whose score to display.
     */
    public ScorePanel(Player player) {

        this.player = player;

        this.setBackground(new Color(255, 255, 255, 160));
        this.setLayout(new BorderLayout(0, 0));

        lblPlayerScore = new JLabel("Score: 0000");
        lblPlayerScore.setFont(new Font("Comic Sans MS", Font.PLAIN, 22));
        lblPlayerScore.setHorizontalAlignment(SwingConstants.CENTER);
        lblPlayerScore.setIcon(new ImageIcon(player.getColor().getMiniSnekImage()));
        lblPlayerScore.setVerticalTextPosition(SwingConstants.CENTER);
        lblPlayerScore.setHorizontalTextPosition(SwingConstants.RIGHT);
        this.add(lblPlayerScore, BorderLayout.CENTER);

        refresh();
    }

    /**
     * Causes the score and death information to be updated from the player object
     */
    public void refresh() {

        String strScore = "" + player.getScore();
        while (strScore.length() < 4) {
            strScore = "0" + strScore;
        }
        lblPlayerScore.setText("Score: " + strScore);

        if (player.getSnek().isDead()) {
            lblPlayerScore.setForeground(new Color(150, 0, 0));
        }

    }

}
