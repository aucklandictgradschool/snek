package ictgradschool.hackathon.amea020.snek;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Represents a snek.
 */
public class Snek implements IGameObject {

    /** Represents all configuration options. Kept in a central place. */
    private final Config cfg = Config.getInstance();

    /**
     * The snek's head. This will in turn contain the references to the rest of the snek.
     */
    private SnekBit head;

    /**
     * How many sections does the snake need to grow right now? Each time we move, if this number is > 0,
     * we'll grow by one and decrement this number.
     */
    private int toGrow;

    /**
     * The direction of the snek's head. The direction of all the other snek parts is determined by what comes
     * before and after.
     */
    private Direction direction;

    /** The color of the snek */
    private SnekColor color;

    /** Whether or not the snek is dead. */
    private boolean isDead;

    /**
     * Creates a new Snek.
     *
     * @param position the initial position of the snek's head
     * @param color the color of the snek
     * @param initialDirection the initial direction the head is facing.
     * @param initialLength the initial length of the snek, NOT including the head.
     */
    public Snek(Point position, SnekColor color, Direction initialDirection, int initialLength) {

        this.toGrow = initialLength;
        this.direction = initialDirection;
        this.head = new SnekBit();
        this.head.position = position;
        this.color = color;

    }

    /**
     * Gets the current direction of the snek's head.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Gets the position of the snek's head.
     */
    public Point getHeadPosition() {
        return head.position;
    }

    /**
     * Sets the current direction of the snek's head.
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Draws the snek's head. This will iterate through and draw the rest of the snek too.
     * @param g
     */
    public void draw(Graphics g) {
        head.draw(g);
    }

    /**
     * Moves the snek according to its current direction. However, sneks can't move if they're dead.
     */
    public void move() {
        if (!isDead) {
            this.head.moveTo(new Point(head.position.x + direction.getDelta().x, head.position.y + direction.getDelta().y));
        }
    }

    /**
     * Tells the snek to grow by a certain amount, which it will do over a certain number of ticks.
     *
     * @param amount
     */
    public void grow(int amount) {
        this.toGrow += amount;
    }

    /**
     * Gets a list of all tiles this snek occupies. Used for collision detection.
     */
    @Override
    public Collection<Point> getOccupiedTiles() {
        ArrayList<Point> list = new ArrayList<>();
        head.addToList(list);
        return list;
    }

    /**
     * Gets a value indicating whether the snek's head collided with any of the given objects.
     *
     * @param other the other game object to check for a collision with the snek's head
     * @return true if the snek's head hit the given object, false otherwise
     */
    public boolean headCollision(IGameObject other) {
        return CollisionUtils.collide(this.head, other);
    }

    /**
     * Gets a value indicating whether the snek's head collided with any of the given objects.
     *
     * @param others the other game objects to check for a collision with the snek's head
     * @return true if the snek's head hit any of the given objects, false otherwise
     */
    public boolean headCollision(Collection<? extends IGameObject> others) {
        return  CollisionUtils.collide(this.head, others);
    }

    /**
     * Gets a value indicating whether the snek's head collided with its body.
     *
     * @return true if the snek hit itself, false otherwise
     */
    public boolean headCollisionWithSelf() {

        SnekBit bit = head.nextBit;
        while (bit != null) {
            if (CollisionUtils.collide(head, bit)) {
                return true;
            }
            bit = bit.nextBit;
        }
        return false;
    }

    /** Gets a value indicating whether the snek is dead. */
    public boolean isDead() {
        return isDead;
    }

    /** Sets whether or not the sneak is dead. */
    public void setDead(boolean dead) {
        isDead = dead;
    }

    /**
     * Represents a single piece of a snek.
     *
     * Contains links to the previous and next bits (similar to a linked list structure).
     */
    private class SnekBit implements IGameObject {

        public SnekBit prevBit, nextBit;
        public Point position;

        /**
         * Moves this bit to the given new position, then updates the next bit in the sequence to move to this one's
         * old position. If the newPosition is the same as the current position, this does nothing.
         *
         * If there is no "next bit", but we need to grow, if we're the "tail" (i.e. last bit), we will create the new
         * bit at this bit's old position.
         *
         * @param newPosition
         */
        public void moveTo(Point newPosition) {
            if (!newPosition.equals(position)) {

                // Set the new position
                Point oldPosition = this.position;
                this.position = newPosition;

                // Update the next bit's position if there is one
                if (nextBit != null) {
                    nextBit.moveTo(oldPosition);
                }

                // Otherwise, if we're the tail and we need to grow, grow by one now.
                else if (toGrow > 0) {
                    toGrow --;
                    this.nextBit = new SnekBit();
                    nextBit.prevBit = this;
                    nextBit.position = oldPosition;
                }
            }
        }

        /**
         * Recursively draws the rest of the snek, then draws this particular snek bit.
         */
        public void draw(Graphics g) {

            // Draw the "rest" of the snek
            if (nextBit != null) {
                nextBit.draw(g);
            }

            // Head
            if (prevBit == null) {
                cfg.getTileset().drawSnekHead(position, direction, color, g);
            }

            // Tail
            else if (nextBit == null) {
                cfg.getTileset().drawSnekTail(position, prevBit.position, color, g);
            }

            // Body
            else {
                cfg.getTileset().drawSnekBody(position, prevBit.position, nextBit.position, color, g);
            }

            // Blood, if dead
            if (isDead) {
                cfg.getTileset().drawBlood(position, g);
            }
        }

        /**
         * Adds this bit to the given list, and recursively adds all subsequent bits to the list.
         */
        public void addToList(Collection<Point> list) {
            list.add(position);
            if (nextBit != null) {
                nextBit.addToList(list);
            }
        }

        /**
         * Gets the single tile this single SnekBit occupies.
         */
        @Override
        public Collection<Point> getOccupiedTiles() {
            return Collections.singleton(position);
        }
    }
}
