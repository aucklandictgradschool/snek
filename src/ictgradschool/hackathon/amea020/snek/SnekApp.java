package ictgradschool.hackathon.amea020.snek;

import javax.swing.*;
import java.awt.*;

/**
 * The program entry point, and the JFrame which displays a {@link MainPanel}.
 */
public class SnekApp extends JFrame {

    /**
     * Creates a new SnekApp.
     */
    public SnekApp() {
        setTitle("Snek");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainPanel frameContent = new MainPanel();
        Container visibleArea = getContentPane();
        visibleArea.add(frameContent);
        setResizable(false);
        pack();
        frameContent.requestFocusInWindow();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(screenSize.width / 2 - getWidth() / 2, screenSize.height / 2 - getHeight() / 2);
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        // A lambda expression. See https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
        SwingUtilities.invokeLater(() -> {
            SnekApp frame = new SnekApp();
            frame.setVisible(true);
        });
    }

}
