package ictgradschool.hackathon.amea020.snek;

/**
 * Different colors of Snek.
 */
public enum SnekColor {

    Green(0, "mini-green-snek.png"),

    Blue(5, "mini-blue-snek.png"),

    Orange(10, "mini-orange-snek.png");

    /** The offset in the tileset where the graphics will be found to render a snek of this color. */
    private final int tilesetXOffs;

    /** The file location of the mini snek image used for the score panels, etc. */
    private final String miniSnekImage;

    /**
     * Creates a new SnekColor.
     *
     * @param tilesetXOffs The offset in the tileset where the graphics will be found to render a snek of this color.
     * @param miniSnekImage The file location of the mini snek image used for the score panels, etc.
     */
    SnekColor(int tilesetXOffs, String miniSnekImage) {
        this.tilesetXOffs = tilesetXOffs;
        this.miniSnekImage = miniSnekImage;
    }

    /** Gets the offset in the tileset where the graphics will be found to render a snek of this color. */
    public int getTilesetXOffs() {
        return tilesetXOffs;
    }

    /** Gets the file location of the mini snek image used for the score panels, etc. */
    public String getMiniSnekImage() {
        return miniSnekImage;
    }
}
