package ictgradschool.hackathon.amea020.snek;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;
import javax.swing.Timer;

/**
 * Represents a game of Snek.
 */
public class SnekGame {

    /** Represents all configuration options. Kept in a central place. */
    private final Config cfg = Config.getInstance();

    /** Listeners */
    private List<ISnekGameListener> listeners = new ArrayList<>();

    /** The players in the game */
    private List<Player> players;

    /** The players in the game who are still alive */
    private List<Player> livingPlayers;

    /** The fruit */
    private List<Fruit> fruit = new ArrayList<>();

    /** The walls */
    private List<Wall> walls = new ArrayList<>();

    /** The sparks */
    private List<Point> sparks = new ArrayList<>();

    /** The timer which will run the game loop */
    private  Timer timer;

    /**
     * Creates and initializes a new game of Snek with a single default player.
     */
    public SnekGame() {
        this(new Player("Player One", SnekColor.Green, PlayerControls.PlayerOne));
    }

    /**
     * Creates and initializes a new game of Snek with the given players.
     *
     * Note: The "..." syntax used here is called "varargs".
     * See: https://docs.oracle.com/javase/1.5.0/docs/guide/language/varargs.html
     */
    public SnekGame(Player... players) {
        this(Arrays.asList(players));
    }

    /** Creates and initializes a new game of Snek with the given players. */
    public SnekGame(List<Player> players) {

        this.players = players;
        this.livingPlayers = new ArrayList<>(players); // A copy of the list

        // Create some walls
        createWalls();

        // Create sneks for each player
        createPlayerSneks();

        // Create the first fruit
        spawnFruit(players.size());
    }

    /**
     * Creates sneks for each player.
     *
     * This method currently only supports one or two players.
     * TODO Add support for more players (up to 4 probably makes sense).
     */
    private void createPlayerSneks() {
        if (players.size() == 1) {

            players.get(0).setSnek(new Snek(
                new Point(cfg.getScreenWidthInTiles() / 2, cfg.getScreenHeightInTiles() / 2),
                players.get(0).getColor(),
                Direction.Up,
                cfg.getInitialSnekLength()));

        }

        else if (players.size() == 2) {

            players.get(0).setSnek(new Snek(
                    new Point(3 * cfg.getScreenWidthInTiles() / 4, 3 * cfg.getScreenHeightInTiles() / 4),
                    players.get(0).getColor(),
                    Direction.Up,
                    cfg.getInitialSnekLength()));

            players.get(1).setSnek(new Snek(
                    new Point(cfg.getScreenWidthInTiles() / 4, cfg.getScreenHeightInTiles() / 4),
                    players.get(1).getColor(),
                    Direction.Up,
                    cfg.getInitialSnekLength()));

        }

        else {
            throw new UnsupportedOperationException("Unsupported number of players. Two-player max.");
        }
    }

    /** Creates walls which serve as obstacles for all sneks. A snek will die if it hits a wall. */
    private void createWalls() {
        walls.add(new Wall(0, 0, cfg.getScreenWidthInTiles(), 1));
        walls.add(new Wall(0, 1, 1, cfg.getScreenHeightInTiles() - 1));
        walls.add(new Wall(1, cfg.getScreenHeightInTiles() - 1, cfg.getScreenWidthInTiles() - 1, 1));
        walls.add(new Wall(cfg.getScreenWidthInTiles() - 1, 1, 1, cfg.getScreenHeightInTiles() - 2));
    }

    /**
     * Allows the game to handle key presses. Delegates to each Player to handle those presses.
     *
     * @param e info about which key was pressed
     */
    public void handleKeyPress(KeyEvent e) {
        for (Player p : players) {
            if (p.handleKeyPress(e)) {
                break;
            }
        }
    }

    /**
     * Starts the game.
     */
    public void start() {
        if (timer == null) {
            // A lambda expression See: https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
            timer = new Timer(70, e -> tick());
            fireGameStartedEvent();
            timer.start();
        }
    }

    /**
     * Stops the game. Once the game is stopped, it cannot be restarted.
     */
    private void stop() {
        if (timer != null) {
            timer.stop();
            fireGameFinishedEvent();
        }
    }

    /**
     * Performs one iteration of the game loop.
     */
    private void tick() {

        // Tick all living players, which will cause their sneks to move
        for (Player p : livingPlayers) {
            p.tick();
        }

        // Check if any sneks have eaten fruit this tick, and increment the scores appropriately.
        int fruitEaten = processFruitEaten();

        // If any sneks ate any fruit, spawn more fruit
        if (fruitEaten > 0) {
            spawnFruit(fruitEaten);
        }

        // See if any currently living players have just died.
        List<Player> deadPlayers = processDeaths();

        // Kill off all dead players' sneks, and inform listeners of deaths
        for (Player deadPlayer : deadPlayers) {
            deadPlayer.getSnek().setDead(true);
            firePlayerEliminatedEvent(deadPlayer);
        }

        // Remove the dead players from the livingPlayers list
        livingPlayers.removeAll(deadPlayers);

        // Inform observers we need to redraw
        fireRedrawEvent();

        // If there are no living players left, we're done.
        if (livingPlayers.isEmpty()) {
            stop();
        }
    }

    /**
     * Checks if any players have died this tick. A player has died this tick if their snek has:
     * <ul>
     *     <li>collided with itself, OR</li>
     *     <li>collided with another snek (dead or alive), OR</li>
     *     <li>collided with a wall</li>
     * </ul>
     *
     * In addition, this method will assign bonus points to any player whose snek blocked another snek, causing it to die.
     *
     * @return the list of players who died this tick
     */
    private List<Player> processDeaths() {
        List<Player> deadPlayers = new ArrayList<>();
        playerLoop:
        for (Player player : livingPlayers) {

            // If a player's snek has collided with themselves, they're dead.
            if (player.getSnek().headCollisionWithSelf()) {
                deadPlayers.add(player);
                continue;
            }

            // If a player's snek has collided with any walls, they're dead.
            else if (player.getSnek().headCollision(walls)) {
                deadPlayers.add(player);
                continue;
            }

            // If a player's snek has collided with any other snek (dead or alive), they're dead.
            // NOTE: Though dead players' sneks can't be controlled, their corpses still remain on the ground
            // as obstacles.
            for (Player otherPlayer : players) {
                if (player != otherPlayer) {
                    if (player.getSnek().headCollision(otherPlayer.getSnek())) {

                        // If the other player's snake isn't already dead, we'll give them bonus points
                        // for getting a kill.
                        if (!otherPlayer.getSnek().isDead()) {
                            incrementScore(otherPlayer, cfg.getKillBonusPoints());
                        }

                        deadPlayers.add(player);
                        continue playerLoop;
                    }
                }
            }
        }

        return deadPlayers;
    }

    /**
     * Goes through all living players, and checks if their sneks have just eaten any fruit (i.e. their heads are
     * at the same co-ordinates of a fruit). If so, performs the following actions:
     * <ul>
     *     <li>Delete that fruit</li>
     *     <li>Increment that player's score</li>
     *     <li>Grow that player's snek</li>
     * </ul>
     *
     * @return the total number of fruit eaten by all sneks
     */
    private int processFruitEaten() {
        int fruitEaten = 0;
        Iterator<Fruit> iter = fruit.iterator();
        while (iter.hasNext()) {

            Fruit f = iter.next();

            // For each living player...
            for (Player player : livingPlayers) {

                // If that player's snek ate the current fruit...
                if (player.getSnek().headCollision(f)) {

                    // Delete the fruit which the snek ate
                    iter.remove();
                    fruitEaten++;

                    // Add a spark (for animation purposes)
                    sparks.add(f.getPosition());

                    // Grow the player's snek
                    player.getSnek().grow(f.getType().getGrowAmount());

                    // Increment that player's score
                    incrementScore(player, f.getType().getPoints());

                    break;

                }
            }
        }
        return fruitEaten;
    }

    /** Draws everything in the game. */
    public void draw(Graphics g) {

        // Draw ground
        cfg.getTileset().drawGrass(new Point(0, 0), cfg.getScreenWidthInTiles(), cfg.getScreenHeightInTiles(), g);

        // Draw walls
        for(Wall w : walls) {
            w.draw(g);
        }

        // Draw the fruit
        for(Fruit f : fruit) {
            f.draw(g);
        }

        // Draw all player sneks
        for (Player p : players) {
            p.getSnek().draw(g);
        }

        // Draw sparks (and clear them)
        Iterator<Point> iter = sparks.iterator();
        while (iter.hasNext()) {
            cfg.getTileset().drawSpark(iter.next(), g);
            iter.remove();
        }
    }

    /**
     * Spawns some fruit.
     *
     * @param numFruit the number of fruit to spawn.
     */
    private void spawnFruit(int numFruit) {

        for (int i = 0; i < numFruit; i++) {

            boolean validPos = false;
            Fruit newFruit = null;
            whileLoop:
            while (!validPos) {

                // Create a new fruit at a random position
                Point pos = new Point((int) (Math.random() * (cfg.getScreenWidthInTiles() - 4)) + 2,
                        (int) (Math.random() * (cfg.getScreenHeightInTiles() - 4)) + 2);
                newFruit = new Fruit(pos);
                validPos = true;

                // Don't allow fruit at the same position as any players' sneks
                for (Player p : players) {
                    if (CollisionUtils.collide(newFruit, p.getSnek())) {
                        validPos = false;
                        continue whileLoop;
                    }
                }

                // Don't allow fruit at the same position as other fruit
                if (CollisionUtils.collide(newFruit, this.fruit)) {
                    validPos = false;
                }

                // Don't allow fruit at the same position as any maze parts
                else if (CollisionUtils.collide(newFruit, this.walls)) {
                    validPos = false;
                }

            }

            // Add the new fruit to the list.
            this.fruit.add(newFruit);

        }
    }

    /**
     * Increments the given player's score by the given amount, and fires an event letting observers know that this
     * score change occurred.
     *
     * @param player the player whose score to increment
     * @param amount the amount to increment
     */
    private void incrementScore(Player player, int amount) {
        player.incrementScore(amount);
        fireScoreChangedEvent(player);
    }

    /** Gets all players in the game. */
    public List<Player> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    /* EVENT GENERATING CODE BELOW */

    /** Adds the given listener to the list of listeners to be notified of game events. */
    public void addGameListener(ISnekGameListener l) {
        this.listeners.add(l);
    }

    /** Removes the given listener from the list of listeners to be notified of game events. */
    public void removeGameListener(ISnekGameListener l) {
        this.listeners.remove(l);
    }

    /** Notifies all listeners that the game needs to be redrawn. */
    private void fireRedrawEvent() {
        fireEvent(new SnekGameEvent(this, SnekGameEvent.EventType.RedrawRequired));
    }

    /** Notifies all listeners that the given player's score has changed. */
    private void fireScoreChangedEvent(Player player) {
        fireEvent(new SnekGameEvent(this, SnekGameEvent.EventType.ScoreChanged, player));
    }

    /** Notifies all listeners that the given player has been eliminated. */
    private void firePlayerEliminatedEvent(Player player) {
        fireEvent(new SnekGameEvent(this, SnekGameEvent.EventType.PlayerEliminated, player));
    }

    /** Notifies all listeners that the game has finished. */
    private void fireGameFinishedEvent() {
        fireEvent(new SnekGameEvent(this, SnekGameEvent.EventType.GameFinished));
    }

    /** Notifies all listeners that the game has started. */
    private void fireGameStartedEvent() {
        fireEvent(new SnekGameEvent(this, SnekGameEvent.EventType.GameStarted));
    }

    /** Notifies all listeners that the given event occurred. */
    private void fireEvent(SnekGameEvent e) {
        for (ISnekGameListener l : listeners) {
            l.onSnekGameEvent(e);
        }
    }
}
