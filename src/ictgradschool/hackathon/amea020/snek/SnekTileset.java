package ictgradschool.hackathon.amea020.snek;

import java.awt.*;

/**
 * Extends the Tileset class with Snek-specific functions.
 */
public class SnekTileset extends Tileset {

    /**
     * Creates a new SnekTileset.
     *
     * @param fileName the name of the image file to use for graphics.
     * @param tileSize the size of each tile on the image, in pixels
     * @param scaledTileSize the size we want to actually draw each tile onscreen, in pixels
     */
    public SnekTileset(String fileName, int tileSize, int scaledTileSize) {
        super(fileName, tileSize, scaledTileSize);
    }

    /**
     * Draws a fruit of the given type at the given co-ordinates.
     *
     * @param pos the onscreen position of the fruit (in tiles)
     * @param type the type of fruit to draw
     * @param g the graphics object used to draw
     */
    public void drawFruit(Point pos, FruitType type, Graphics g) {

        drawTileAtTilePosition(pos, type.getTileX(), type.getTileY(), g);
    }

    /**
     * Draws the head of the snek.
     *
     * @param pos the onscreen position of the head (in tiles)
     * @param direction the direction of the head
     * @param color the color of the snek
     * @param g the graphics object used to draw
     */
    public void drawSnekHead(Point pos, Direction direction, SnekColor color, Graphics g) {

        int tileX = 0, tileY = 0;

        switch (direction) {
            case None:
            case Up:
                tileX = 3; tileY = 0; break;
            case Down:
                tileX = 4; tileY = 1; break;
            case Left:
                tileX = 3; tileY = 1; break;
            case Right:
                tileX = 4; tileY = 0; break;
        }

        drawTileAtTilePosition(pos, tileX + color.getTilesetXOffs(), tileY, g);
    }

    /**
     * Draws the snek's tail at the given tile position onscreen. The direction of the tail is determined by the
     * position of the previous snek segment.
     *
     * @param pos the onscreen tile-based coordinates of the tail
     * @param prevPos the onscreen tile-based coordinates of the snek bit just before the tail. Used to determine direction.
     * @param color the color of the snek
     * @param g the graphics object used to draw
     */
    public void drawSnekTail(Point pos, Point prevPos, SnekColor color, Graphics g) {

        int tileX = 0, tileY = 0;

        if (prevPos.x == pos.x) {

            // Up
            if (prevPos.y < pos.y) {
                tileX = 3;
                tileY = 2;
            }

            // Down
            else {
                tileX = 4;
                tileY = 3;
            }

        }

        else if (prevPos.y == pos.y) {

            // Left
            if (prevPos.x < pos.x) {
                tileX = 3;
                tileY = 3;
            }

            // Right
            else {
                tileX = 4;
                tileY = 2;
            }

        }

        drawTileAtTilePosition(pos, tileX + color.getTilesetXOffs(), tileY, g);

    }

    /**
     * Draws a body section of the snek. Exactly which body section will be drawn will depend on the previous and
     * next sections (so they connect to each other).
     *
     * @param pos the onscreen position of the section to draw (in tiles)
     * @param prevPos the onscreen position of the previous section (in tiles)
     * @param nextPos the onscreen position of the next section (in tiles)
     * @param color the color of the snek
     * @param g the graphics object used to draw
     */
    public void drawSnekBody(Point pos, Point prevPos, Point nextPos, SnekColor color, Graphics g) {

        int tileX, tileY;

        // straight up-down
        if (prevPos.x == nextPos.x && prevPos.x == pos.x) {
            tileX = 2;
            tileY = 1;
        }

        // straight left-right
        else if (prevPos.y == nextPos.y && prevPos.y == pos.y) {
            tileX = 1;
            tileY = 0;
        }

        // top-left corner
        else if (prevPos.y > pos.y && prevPos.x == pos.x && nextPos.x > pos.x && nextPos.y == pos.y ||
                nextPos.y > pos.y && nextPos.x == pos.x && prevPos.x > pos.x && prevPos.y == pos.y) {

            tileX = 0;
            tileY = 0;
        }

        // top-right corner
        else if (prevPos.x < pos.x && prevPos.y == pos.y && nextPos.y > pos.y && nextPos.x == pos.x ||
                nextPos.x < pos.x && nextPos.y == pos.y && prevPos.y > pos.y && prevPos.x == pos.x) {

            tileX = 2;
            tileY = 0;
        }

        // bottom-left corner
        else if (prevPos.y < pos.y && prevPos.x == pos.x && nextPos.x > pos.x && nextPos.y == pos.y ||
                nextPos.y < pos.y && nextPos.x == pos.x && prevPos.x > pos.x && prevPos.y == pos.y) {

            tileX = 0;
            tileY = 1;
        }

        // bottom-right corner
        else if (prevPos.x < pos.x && prevPos.y == pos.y && nextPos.y < pos.y && nextPos.x == pos.x ||
                nextPos.x < pos.x && nextPos.y == pos.y && prevPos.y < pos.y && prevPos.x == pos.x) {

            tileX = 2;
            tileY = 2;
        }

        else {
            tileX = -1;
            tileY = -1;
            System.err.println("ERROR! prevPos: " + prevPos + ", pos: " + pos + ", nextPos: " + nextPos);
        }

        drawTileAtTilePosition(pos, tileX + color.getTilesetXOffs(), tileY, g);

    }

    /**
     * Draws blood splatter at the given tile position.
     *
     * @param pos
     * @param g
     */
    public void drawBlood(Point pos, Graphics g) {
        drawTileAtTilePosition(pos, 0, 2, g);
    }

    /**
     * Draws a spark at the given tile position.
     *
     * @param pos
     * @param g
     */
    public void drawSpark(Point pos, Graphics g) {
        drawTileAtTilePosition(pos, 1, 1, g);
    }

    /**
     * Draws a brick at the given tile position.
     *
     * @param pos
     * @param g
     */
    public void drawBrick(Point pos, Graphics g) {
        drawTileAtTilePosition(pos, 2, 3, g);
    }

    /**
     * Draws the given amount of grass at the given tile position.
     *
     * @param pos the tile co-ordinates of the top-left grass block
     * @param g the graphics
     */
    public void drawGrass(Point pos, int width, int height, Graphics g) {

        final int NUM_GRASS_X = 4;
        final int NUM_GRASS_Y = 4;

        for (int x = pos.x; x < width + pos.x; x+=NUM_GRASS_X) {

            for (int y = pos.y; y < height + pos.y; y+=NUM_GRASS_Y) {

                drawTiles(x * Config.getInstance().getTileSize(), y * Config.getInstance().getTileSize(), 0, 4, 4, 4, g);

            }

        }

    }

    /**
     * Draws the given tile in the tileset (tileX, tileY) at the given tile position onscreen.
     *
     * @param pos the onscreen position (in tiles)
     * @param tileX the x index of the tileset
     * @param tileY the y index of the tileset
     * @param g the graphics object used to draw
     */
    private void drawTileAtTilePosition(Point pos, int tileX, int tileY, Graphics g) {
        drawTile(pos.x * Config.getInstance().getTileSize(), pos.y * Config.getInstance().getTileSize(), tileX, tileY, g);
    }
}
