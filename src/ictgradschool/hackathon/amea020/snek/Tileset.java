package ictgradschool.hackathon.amea020.snek;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Stores an image and allows us to draw portions (tiles) of that image.
 */
public class Tileset {

    /** The size of each tile, in pixels */
    private final int tileSize;

    /** the image containing all the tiles to draw */
    private final Image tiles;

    /**
     * Creates the tileset
     *
     * @param fileName the name of the graphics file
     * @param tileSize the size, in pixels, of each tile, in the graphics file
     * @param scaledTileSize the size, in pixels, we want to actually draw each tile
     */
    public Tileset(String fileName, int tileSize, int scaledTileSize) {
        try {
            Image tiles = ImageIO.read(new File(fileName));
            if (tileSize == scaledTileSize) {
                this.tiles = tiles;
                this.tileSize = tileSize;
            }

            // If we need to scale the image, do it once, now.
            // This is a lot faster than scaling each time we want to draw a tile.
            else {
                double scaleFactor = (double) scaledTileSize / (double) tileSize;
                int newWidth = (int) (tiles.getWidth(null) * scaleFactor);
                int newHeight = (int) (tiles.getHeight(null) * scaleFactor);
                this.tiles = tiles.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
                this.tileSize = scaledTileSize;
            }

        } catch (IOException e) {

            // Should never happen - unrecoverable.
            throw new RuntimeException(e);
        }
    }

    /**
     * Draws a single tile in the tileset using the given Graphics object.
     *
     * @param destX The x coordinate, in pixels, where the tile should be drawn
     * @param destY The y coordinate, in pixels, where the tile should be drawn
     * @param tileX The X index into the tileset
     * @param tileY The Y index into the tileset
     * @param g The Graphics object used to draw the thing
     */
    public void drawTile(int destX, int destY, int tileX, int tileY, Graphics g) {

        drawTiles(destX, destY, tileX, tileY, 1, 1, g);

    }

    /**
     * Draws a group of tiles in the tileset using the given Graphics object.
     *
     * @param destX The x coordinate, in pixels, where the top-left tile should be drawn
     * @param destY The y coordinate, in pixels, where the top-left tile should be drawn
     * @param tileX The X index into the tileset
     * @param tileY The Y index into the tileset
     * @param ntX The number of tiles in the X direction
     * @param ntY the number of tiles in the Y direction
     * @param g The Graphics object used to draw the thing
     */
    public void drawTiles(int destX, int destY, int tileX, int tileY, int ntX, int ntY, Graphics g) {

        int srcX = tileX * tileSize;
        int srcY = tileY * tileSize;
        int width = ntX * tileSize;
        int height = ntY * tileSize;

        g.drawImage(tiles, destX, destY, destX + width, destY + height,
                srcX, srcY, srcX + width, srcY + height, null);


    }

}
