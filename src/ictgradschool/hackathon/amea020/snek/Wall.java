package ictgradschool.hackathon.amea020.snek;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Represents a wall that can't be passed. You will die if you run into this.
 */
public class Wall implements IGameObject {

    private final Config cfg = Config.getInstance();

    /** The top-left position of the wall */
    private final Point position;

    /** The width and height of the wall */
    private final Dimension size;

    /** Contains a list of all tiles this wall occupies. Used for collision detection. */
    private final Collection<Point> occupiedTiles;

    /**
     * Creates a new wall
     *
     * @param x the lefthand co-ordinate, in tiles
     * @param y the top co-ordinate, in tiles
     * @param width the width, in tiles
     * @param height the height, in tiles
     */
    public Wall(int x, int y, int width, int height) {
        this(new Point(x, y), new Dimension(width, height));
    }

    /**
     * Creates a new wall
     *
     * @param position the top-lefthand corner position, in tiles
     * @param size the width & height, in tiles
     */
    public Wall(Point position, Dimension size) {
        this.position = position;
        this.size = size;

        ArrayList<Point> tiles = new ArrayList<>();
        for (int x = position.x; x < position.x + size.width; x++) {
            for (int y = position.y; y < position.y + size.height; y++) {
                tiles.add(new Point(x, y));
            }
        }
        this.occupiedTiles = Collections.unmodifiableCollection(tiles);
    }

    /**
     * Draws the wall using the given graphics object. A wall is drawn by drawing a single wall tile in each of this
     * wall's "occupied tiles" list.
     */
    public void draw(Graphics g) {
        for (Point p : occupiedTiles) {
            cfg.getTileset().drawBrick(p, g);
        }
    }

    /** Gets the top-left position of the wall */
    public Point getPosition() {
        return position;
    }

    /** Gets the width and height of the wall */
    public Dimension getSize() {
        return size;
    }

    /** Gets a list of all tiles this wall occupies. Used for collision detection. */
    @Override
    public Collection<Point> getOccupiedTiles() {
        return this.occupiedTiles;
    }
}
